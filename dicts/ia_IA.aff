﻿##################################################################
# Interlingua - Regulas de affixo                                #
# (c) 2009-2013 Artek Dzialek(artur<point>dzialek<at>gazeta.pl)  #
# Iste software es licentiate secundo GPL 3.                     #
##################################################################

VERSION 2013.05.06
SET UTF-8
FLAG UTF-8

TRY eaiortncslpmuhdgbyfvxqzkjwàáâäãāăạąçèéêëēĕẹęìíîïĩīĭįñòóôöõōŏọǫùúûüũūŭụųỳýŷÿỹȳỵ
KEY qwa|wes|erd|rtf|tyg|yuh|uij|iok|opl|asz|sdx|dfc|fgv|ghb|hjn|jkm|kl|zxs|xcd|cvf|vbg|bnh|nmj

WORDCHARS 0123456789'-

#NOSPLITSUGS
NOSUGGEST !
NEEDAFFIX _

COMPOUNDBEGIN I
COMPOUNDEND J
ONLYINCOMPOUND K

COMPOUNDPERMITFLAG W
COMPOUNDMIN 2
COMPOUNDRULE 1
COMPOUNDRULE Y*Z

MAP 16
MAP aàáâäãāăạą
MAP cç
MAP eèéêëēĕẹę
MAP iìíîïĩīĭį
MAP nñ
MAP oòóôöõōŏọǫ
MAP uùúûüũūŭụų
MAP yỳýŷÿỹȳỵ
MAP AÀÁÂÄÃĀĂẠĄ
MAP CÇ
MAP EÈÉÊËĒĔẸĘ
MAP IÌÍÎÏĨĪĬĮ
MAP NÑ
MAP OÒÓÔÖÕŌŎỌǪ
MAP UÙÚÛÜŨŪŬỤŲ
MAP YỲÝŶŸỸȲỴ

REP 58
REP o au
REP au o
REP c qu
REP qu c
REP ch c
REP c ch
REP ch k
REP k ch
REP ch q
REP q ch
REP ph f
REP f ph
REP rh r
REP r rh
REP th t
REP t th
REP x ct
REP ct x
REP bb b
REP b bb
REP cc c
REP c cc
REP ff f
REP f ff
REP ll l
REP l ll
REP mm m
REP m mm
REP nn n
REP n nn
REP pp p
REP p pp
REP rr r
REP r rr
REP ss s
REP s ss
REP ss c
REP c ss
REP tt t
REP t tt
REP z zz
REP zz z
REP oo u
REP u oo
REP ea i
REP i ea
REP a$ e
REP a$ o
REP e$ a
REP e$ o
REP o$ a
REP o$ e
REP as$ es
REP as$ os
REP es$ as
REP es$ os
REP os$ as
REP os$ es

SFX A Y 25
SFX A  0  0    .   ds:e po:adj
SFX A  0  0    [^i]  ds:e is:sg po:sub
SFX A  0  e    i   ds:e is:sg po:sub
SFX A  0  es    [^ec]  ds:e is:pl po:sub
SFX A  0  s    e   ds:e is:pl po:sub
SFX A  0  hes    c   ds:e is:pl po:sub
SFX A  e  o    e   ds:o is:sg po:sub
SFX A  e  a    e   ds:a is:sg po:sub
SFX A  e  os    e   ds:o is:pl po:sub
SFX A  e  as    e   ds:a is:pl po:sub
SFX A  0  o    [^e]  ds:o is:sg po:sub
SFX A  0  a    [^e]  ds:a is:sg po:sub
SFX A  0  os    [^e]  ds:o is:pl po:sub
SFX A  0  as    [^e]  ds:a is:pl po:sub
SFX A  e  o    e   ds:o po:adv
SFX A  0  o    [^e]  ds:o po:adv
SFX A  0  mente   [^c]  ds:mente po:adv
SFX A  0  amente   c   ds:mente po:adv
SFX A  0  -    [^c]  ds:mente po:adv
SFX A  0  a-    c   ds:mente po:adv
SFX A  0  issime/_Á  [^eic]  ds:issim
SFX A  e  issime/_Á  [^i]e  ds:issim
SFX A  e  ssime/_Á  ie   ds:issim
SFX A  0  ssime/_Á  i   ds:issim
SFX A  0  hissime/_Á  c   ds:issim
# Alternative suffix for "-amente" adverbs
# SFX A  e  amente   e   ds:amente po:adv
# SFX A  0  amente   [^e]  ds:amente po:adv
# SFX A  e  a-    e   ds:amente po:adv
# SFX A  0  a-    [^e]  ds:amente po:adv
# Flag for adjectives and derived nouns ending with -o and -a 

SFX Á Y 20
SFX Á  0  0    .   ds:e po:adj
SFX Á  0  0    [^i]  ds:e is:sg po:sub
SFX Á  0  e    i   ds:e is:sg po:sub
SFX Á  0  es    [^ec]  ds:e is:pl po:sub
SFX Á  0  s    e   ds:e is:pl po:sub
SFX Á  0  hes    c   ds:e is:pl po:sub
SFX Á  e  o    e   ds:o is:sg po:sub
SFX Á  e  a    e   ds:a is:sg po:sub
SFX Á  e  os    e   ds:o is:pl po:sub
SFX Á  e  as    e   ds:a is:pl po:sub
SFX Á  0  o    [^e]  ds:o is:sg po:sub
SFX Á  0  a    [^e]  ds:a is:sg po:sub
SFX Á  0  os    [^e]  ds:o is:pl po:sub
SFX Á  0  as    [^e]  ds:a is:pl po:sub
SFX Á  e  o    e   ds:o po:adv
SFX Á  0  o    [^e]  ds:o po:adv
SFX Á  0  mente   [^c]  ds:mente po:adv
SFX Á  0  amente   c   ds:mente po:adv
SFX Á  0  -    [^c]  ds:mente po:adv
SFX Á  0  a-    c   ds:mente po:adv
# Alternative suffix for "-amente" adverbs
# SFX Á  e  amente   e   ds:amente po:adv
# SFX Á  0  amente   [^e]  ds:amente po:adv
# SFX Á  e  a-    e   ds:amente po:adv
# SFX Á  0  a-    [^e]  ds:amente po:adv
# Continuation flag for derived adjectives

SFX B Y 1
SFX B  0  0    .   po:adj
#SFX B  0  mente   a   ds:amente po:adv 
# Flag for adjectives derived by conversion from nouns, invariable

SFX C Y 27
SFX C  0  0    [cte]   ds:e po:adj
SFX C  0  0    [cte]   ds:e is:sg po:sub
SFX C  0  ches   c   ds:e is:pl po:sub
SFX C  0  co    c   ds:o is:sg po:sub
SFX C  0  ca    c   ds:a is:sg po:sub
SFX C  0  cos    c   ds:o is:pl po:sub
SFX C  0  cas    c   ds:a is:pl po:sub
SFX C  0  co    c   ds:o po:adv
SFX C  0  camente   c   ds:mente po:adv
SFX C  0  ca-    c   ds:mente po:adv
SFX C  0  chissime/_Á  c   ds:issim
SFX C  0  tes   t   ds:e is:pl po:sub
SFX C  0  to    t   ds:o is:sg po:sub
SFX C  0  ta    t   ds:a is:sg po:sub
SFX C  0  tos    t   ds:o is:pl po:sub
SFX C  0  tas    t   ds:a is:pl po:sub
SFX C  0  to    t   ds:o po:adv
SFX C  0  mente   [te]   ds:mente po:adv
SFX C  0  -    [te]   ds:mente po:adv
SFX C  0  tissime/_Á  t   ds:issim
SFX C  0  s   e   ds:e is:pl po:sub
SFX C  e  io    e   ds:o is:sg po:sub
SFX C  e  ia    e   ds:a is:sg po:sub
SFX C  e  ios    e   ds:o is:pl po:sub
SFX C  e  ias    e   ds:a is:pl po:sub
SFX C  e  io    e   ds:o po:adv
SFX C  e  issime/_Á  e   ds:issim
# adjectives doubling the final consonant ric, sic, mat and -sage, -vage

SFX D Y 1
SFX D  0  0    .   po:adv

SFX E Y 8
SFX E  0  0    .   ds:e po:adj
SFX E  0  0    .   ds:e is:sg po:sub
SFX E  0  s    e   ds:e is:pl po:sub
SFX E  0  es    [^e]  ds:e is:pl po:sub
SFX E  0  -    .   ds:mente po:adv
SFX E  0  mente   .   ds:mente po:adv
SFX E  0  issime/_Á  [^e]  ds:issime
SFX E  e  issime/_Á  e   ds:issime
# Alternative rules for comperative forms
#SFX E  0  issime/_Á  [^elr]  ds:issime
#SFX E  e  issime/_Á  e   ds:issime
#SFX E  0  lime/_Á   l   ds:issime
#SFX E  0  rime/_Á   r   ds:issime
# Flag for adjectives from the "purist list", following "third declension"

SFX É Y 5
SFX É  0  0    e   po:adj
SFX É  0  0    e   ds:e is:sg po:sub
SFX É  0  s    e   ds:e is:pl po:sub
SFX É  0  -    e   ds:mente po:adv
SFX É  0  mente   e   ds:mente po:adv
# Continuation flag for derived adjectives ending with -nte, -bile

SFX F Y 2
SFX F  o  a/_S   o   ds:a
SFX F  or  rice/_S   or  ds:a
 
SFX G Y 1
SFX G  0  0    .   is:sg po:sub

SFX L Y 1
SFX L  0  0    .   is:pl po:sub

SFX O Y 9
SFX O  0  0    e   ds:e po:adj
SFX O  0  0    e   ds:e is:sg po:sub
SFX O  0  s    e   ds:e is:pl po:sub
SFX O  e  a    e   ds:a is:sg po:sub
SFX O  e  as    e   ds:a is:pl po:sub
SFX O  e  o    e   ds:o is:sg po:sub
SFX O  e  os    e   ds:o is:pl po:sub
SFX O  e  o    e   ds:o po:adv
SFX O  0  mente   .   ds:mente po:adv

SFX P Y 3
SFX P  0  0    .   is:sg po:pron
SFX P  0  s    [aeiou]  is:pl po:pron
SFX P  0  es    [^aeiou] is:pl po:pron

SFX Q Y 7
SFX Q  0  0    .   ds:e po:adj
SFX Q  0  0/_P   [^iur]  ds:e 
SFX Q  0  e/_P   [iur]  ds:e
SFX Q  e  o/_P   e   ds:o
SFX Q  e  a/_P   e   ds:a
SFX Q  0  o/_P   [^e]  ds:o
SFX Q  0  a/_P   [^e]  ds:a

SFX R Y 2
SFX R  0  0    .   ds:e po:adj
SFX R  0  0/_P   .   ds:e

SFX S Y 4
SFX S  0  0    .   is:sg po:sub
SFX S  0  s    [aeiouy] is:pl po:sub
SFX S  0  es    [^caeiouy] is:pl po:sub
SFX S  0  hes    c   is:pl po:sub

SFX T Y 3
SFX T  0  0     .  is:sg po:sub
SFX T  is es    .  is:pl po:sub
SFX T  s  des   .  is:pl po:sub

SFX U Y 8
SFX U  0  0    .   is:sg po:sub
SFX U  is e    [aeiouyp]sis is:sg po:sub
SFX U  is es   is is:pl po:sub
SFX U  y  ies  [^aeiou]y is:pl po:sub
SFX U  0  s    [aeiou]y is:pl po:sub
SFX U  0  s    [^su] is:pl po:sub
SFX U  0  s    [^ae]u  is:pl po:sub
SFX U  0  x    [ae]u  is:pl po:sub

SFX V Y 48
SFX V  0  0/_S   ar   ds:ar
SFX V  0  0/_S   er   ds:er
SFX V  0  0/_S   ir   ds:ir
SFX V  0  0    ar   ds:ar is:inf po:verb
SFX V  0  0    er   ds:er is:inf po:verb
SFX V  0  0    ir   ds:ir is:inf po:verb
SFX V  r  0    ar   ds:a is:pres po:verb
SFX V  r  0    er   ds:e is:pres po:verb
SFX V  r  0    ir   ds:i is:pres po:verb
SFX V  r  va    ar   ds:ava is:pret po:verb
SFX V  r  va    er   ds:eva is:pret po:verb
SFX V  r  va    ir   ds:iva is:pret po:verb
SFX V  0  a    ar   ds:ara is:fut po:verb
SFX V  0  a    er   ds:era is:fut po:verb
SFX V  0  a    ir   ds:ira is:fut po:verb
SFX V  0  ea    ar   ds:area is:cond po:verb
SFX V  0  ea    er   ds:erea is:cond po:verb
SFX V  0  ea    ir   ds:irea is:cond po:verb
SFX V  r  ntissime/_Á  ar   ds:ant is:pres ds:issim
SFX V  r  ntissime/_Á  [^pc]er  ds:ent is:pres ds:issim
SFX V  r  ntissime/_Á  [^ai][pc]er ds:ent is:pres ds:issim
SFX V  r  ntissime/_Á  [^f]acer ds:ent is:pres ds:issim
SFX V  r  ntissime/_Á  [^fjp]icer ds:ent is:pres ds:issim
SFX V  r  ntissime/_Á  [^cs]aper ds:ent is:pres ds:issim
SFX V  r  ntissime/_Á  [^c]iper ds:ent is:pres ds:issim
SFX V  er  ientissime/_Á facer  ds:ient is:pres ds:issim
SFX V  er  ientissime/_Á [fjp]icer ds:ient is:pres ds:issim
SFX V  er  ientissime/_Á [cs]aper ds:ient is:pres ds:issim
SFX V  er  ientissime/_Á ciper  ds:ient is:pres ds:issim
SFX V  r  entissime/_Á ir   ds:ient is:pres ds:issim
SFX V  r  te/_Á   ar   ds:at is:perf 
SFX V  er  ite/_Á   er   ds:it is:perf 
SFX V  r  te/_Á   ir   ds:it is:perf 
SFX V  r  tissime/_Á  ar   ds:at is:perf ds:issim
SFX V  er  itissime/_Á  er   ds:it is:perf ds:issim
SFX V  r  tissime/_Á  ir   ds:it is:perf ds:issim
SFX V  r  nte/_É   ar   ds:ant is:pres
SFX V  r  nte/_É   [^pc]er  ds:ent is:pres
SFX V  r  nte/_É   [^ai][pc]er ds:ent is:pres
SFX V  r  nte/_É   [^f]acer ds:ent is:pres
SFX V  r  nte/_É   [^fjp]icer ds:ent is:pres
SFX V  r  nte/_É   [^cs]aper ds:ent is:pres
SFX V  r  nte/_É   [^c]iper ds:ent is:pres
SFX V  er  iente/_É   facer  ds:ient is:pres
SFX V  er  iente/_É   [fjp]icer ds:ient is:pres
SFX V  er  iente/_É   [cs]aper ds:ient is:pres
SFX V  er  iente/_É   ciper  ds:ient is:pres
SFX V  r  ente/_É   ir   ds:ient is:pres

SFX X Y 1
SFX X  0  0/zIKW   .   

SFX a Y 3
SFX a  r  ntia/_S   ar  ds:antia
SFX a  r  ntia/_S   er  ds:entia
SFX a  r  entia/_S  ir  ds:ientia

SFX b Y 10
SFX b  r  bile/_É   ar  ds:abil
SFX b  er  ibile/_É  er  ds:ibil
SFX b  r  bile/_É   ir  ds:ibil
SFX b  -  ibile/_É  t-  ds:ibil
SFX b  -  ibile/_É  [sx]- ds:ibil
SFX b  r  bilissime/_Á ar  ds:abil ds:issim
SFX b  er  ibilissime/_Á er  ds:ibil ds:issim
SFX b  r  bilissime/_Á ir  ds:ibil ds:issim
SFX b  -  ibilissime/_Á t-  ds:ibil ds:issim
SFX b  -  ibilissime/_Á [sx]- ds:ibil ds:issim

SFX c Y 5
SFX c  r  bilitate/_S  ar  ds:abil ds:itate
SFX c  er  ibilitate/_S er  ds:ibil ds:itate
SFX c  r  bilitate/_S  ir  ds:ibil ds:itate
SFX c  -  ibilitate/_S t-  ds:ibil ds:itate
SFX c  -  ibilitate/_S [sx]- ds:ibil ds:itate

SFX d Y 4
SFX d  r  da/_S   ar  ds:ada
SFX d  er  ada/_S   [^c]er ds:ada
SFX d  er  iada/_S   cer  ds:ada
SFX d  ir  ada/_S   ir  ds:ada

SFX e Y 4
SFX e  ar  eria/_S   [^c]ar ds:eria
SFX e  ar  heria/_S  car  ds:eria
SFX e  0  ia/_S   er  ds:eria
SFX e  ir  eria/_S   ir  ds:eria

SFX f Y 45
SFX f  a  esime   a   ds:esim ds:e po:adj
SFX f  0  sime   e   ds:esim ds:e po:adj
SFX f  i  esime   i   ds:esim ds:e po:adj
SFX f  o  esime   o   ds:esim ds:e po:adj
SFX f  0  esime   [^aeio]  ds:esim ds:e po:adj
SFX f  a  esime   a   ds:esim is:sg po:sub
SFX f  0  sime   e   ds:esim is:sg po:sub
SFX f  i  esime   i   ds:esim is:sg po:sub
SFX f  o  esime   o   ds:esim is:sg po:sub
SFX f  0  esime   [^aeio]  ds:esim is:sg po:sub
SFX f  a  esimes   a   ds:esim is:pl po:sub
SFX f  0  simes   e   ds:esim is:pl po:sub
SFX f  i  esimes   i   ds:esim is:pl po:sub
SFX f  o  esimes   o   ds:esim is:pl po:sub
SFX f  0  esimes   [^aeio]  ds:esim is:pl po:sub
SFX f  a  esimo   a   ds:esim ds:o is:sg po:sub
SFX f  0  simo   e   ds:esim ds:o is:sg po:sub
SFX f  i  esimo   i   ds:esim ds:o is:sg po:sub
SFX f  o  esimo   o   ds:esim ds:o is:sg po:sub
SFX f  0  esimo   [^aeio]  ds:esim ds:o is:sg po:sub
SFX f  a  esimos   a   ds:esim ds:o is:pl po:sub
SFX f  0  simos   e   ds:esim ds:o is:pl po:sub
SFX f  i  esimos   i   ds:esim ds:o is:pl po:sub
SFX f  o  esimos   o   ds:esim ds:o is:pl po:sub
SFX f  0  esimos   [^aeio]  ds:esim ds:o is:pl po:sub
SFX f  a  esima   a   ds:esim ds:a is:sg po:sub
SFX f  0  sima   e   ds:esim ds:a is:sg po:sub
SFX f  i  esima   i   ds:esim ds:a is:sg po:sub
SFX f  o  esima   o   ds:esim ds:a is:sg po:sub
SFX f  0  esima   [^aeio]  ds:esim ds:a is:sg po:sub
SFX f  a  esimas   a   ds:esim ds:a is:pl po:sub
SFX f  0  simas   e   ds:esim ds:a is:pl po:sub
SFX f  i  esimas   i   ds:esim ds:a is:pl po:sub
SFX f  o  esimas   o   ds:esim ds:a is:pl po:sub
SFX f  0  esimas   [^aeio]  ds:esim ds:a is:pl po:sub
SFX f  a  esimo   a   ds:esim ds:o po:adv
SFX f  0  simo   e   ds:esim ds:o po:adv
SFX f  i  esimo   i   ds:esim ds:o po:adv
SFX f  o  esimo   o   ds:esim ds:o po:adv
SFX f  0  esimo   [^aeio]  ds:esim ds:o po:adv
SFX f  a  esimemente  a   ds:esim ds:mente po:adv
SFX f  0  simemente  e   ds:esim ds:mente po:adv
SFX f  i  esimemente  i   ds:esim ds:mente po:adv
SFX f  o  esimemente  o   ds:esim ds:mente po:adv
SFX f  0  esimemente  [^aeio]  ds:esim ds:mente po:adv

SFX g Y 3
SFX g  r  ge/_S   ar  ds:age
SFX g  er  age/_S   er  ds:age
SFX g  ir  age/_S   ir  ds:age

SFX h Y 4
SFX h  a  ena/_S   a  ds:ena
SFX h  o  ena/_S   o  ds:ena
SFX h  0  na/_S   e  ds:ena
SFX h  i  ena/_S   i  ds:ena

SFX i Y 10
SFX i  r  tori/_Á   ar  ds:atori
SFX i  er  itori/_Á  er  ds:itori
SFX i  r  tori/_Á   ir  ds:itori
SFX i  -  ori/_Á   t-  ds:ori
SFX i  -  ori/_Á   [sx]- ds:ori
SFX i  r  torissime/_Á ar  ds:atori
SFX i  er  itorissime/_Á er  ds:itori
SFX i  r  torissime/_Á ir  ds:itori
SFX i  -  orissime/_Á  t-  ds:ori
SFX i  -  orissime/_Á  [sx]- ds:ori

SFX j Y 5
SFX j  r  torio/_S  ar  ds:atorio
SFX j  er  itorio/_S  er  ds:itorio
SFX j  r  torio/_S  ir  ds:itorio
SFX j  -  orio/_S   t-  ds:orio
SFX j  -  orio/_S   [sx]- ds:orio

SFX k Y 4
SFX k  0  itate/_S  [^ie] ds:itate 
SFX k  e  itate/_S  [^i]e ds:itate 
SFX k  0  tate/_S   ie  ds:itate 
SFX k  0  etate/_S  i  ds:itate 

SFX l Y 10
SFX l  ple  llion/_Sf  iple ds:illion
SFX l  uple illion/_Sf  uple ds:illion
SFX l  a  illion/_zSf  a  ds:illion
SFX l  0  llion/_zSf  i  ds:illion
SFX l  ple  lliardo/_Sf  iple ds:illiardo
SFX l  uple illiardo/_Sf uple ds:illiardo
SFX l  a  illiardo/_zSf a  ds:illiardo
SFX l  0  lliardo/_zSf i  ds:illiardo
SFX l  0  llion/_JSf  enti ds:illion
SFX l  0  lliardo/_JSf enti ds:illiardo

SFX m Y 3
SFX m  r  mento/_S  ar  ds:amento
SFX m  er  imento/_S  er  ds:imento
SFX m  r  mento/_S  ir  ds:imento

SFX n Y 5
SFX n  r  tion/_S   ar  ds:ation
SFX n  er  ition/_S  er  ds:ition
SFX n  r  tion/_S   ir  ds:ition
SFX n  -  ion/_S   t-  ds:ion
SFX n  -  ion/_S   [sx]- ds:ion

SFX o Y 9
SFX o  r  tor/_S   ar  ds:ator
SFX o  er  itor/_S   er  ds:itor
SFX o  r  tor/_S   ir  ds:itor
SFX o  -  or/_S   t-  ds:or
SFX o  -  or/_S   [sx]- ds:or
SFX o  r  trice/_S  ar  ds:atrice
SFX o  er  itrice/_S  er  ds:itrice
SFX o  r  trice/_S  ir  ds:itrice
SFX o  -  rice/_S   t-  ds:rice
# SFX o  -  rice/_S   [sx]- ds:rice

SFX p Y 4
SFX p  a  uple/_O   a  ds:upl
SFX p  o  uple/_O   o  ds:upl
SFX p  e  uple/_O   e  ds:upl
SFX p  i  uple/_O   i  ds:upl

SFX q Y 6
SFX q  a  c/_Á   ia  ds:ic
SFX q  smo  c/_Á   ismo ds:ic
SFX q  a  ic/_Á   [^i]a ds:ic
SFX q  o  ic/_Á   [^i]smo ds:ic
SFX q  o  ic/_Á   [^s]mo ds:ic
SFX q  o  ic/_Á   [^m]o ds:ic

SFX r Y 6
SFX r  a  citate/_S  ia  ds:ic ds:itate
SFX r  smo  citate/_S  ismo ds:ic ds:itate
SFX r  a  icitate/_S  [^i]a ds:ic ds:itate
SFX r  o  icitate/_S  [^i]smo ds:ic ds:itate
SFX r  o  icitate/_S  [^s]mo ds:ic ds:itate
SFX r  o  icitate/_S  [^m]o ds:ic ds:itate

SFX s Y 2
SFX s  -  itate/_S  t-  ds:tiv ds:itate
SFX s  -  itate/_S  [sx]- ds:siv ds:itate

SFX t Y 5
SFX t  -  e/_Á   t-  ds:e
SFX t  -  e/_Á   [sx]- ds:e
SFX t  -  issime/_Á  t-  ds:e ds:issim
SFX t  -  issime/_Á  [sx]- ds:e ds:issim
SFX t  0  e    er  ds:er ds:e is:inf

SFX u Y 5
SFX u  r  tura/_S   ar  ds:atura
SFX u  er  itura/_S  er  ds:itura
SFX u  r  tura/_S   ir  ds:itura
SFX u  -  ura/_S   t-  ds:ura
SFX u  -  ura/_S   [sx]- ds:ura

SFX v Y 10
SFX v  r  tive/_Á   ar  ds:ativ
SFX v  er  itive/_Á  er  ds:itiv
SFX v  r  tive/_Á   ir  ds:itiv
SFX v  -  ive/_Á   t-  ds:tive
SFX v  -  ive/_Á   [sx]- ds:sive
SFX v  r  tivissime/_Á ar  ds:ativ ds:issim
SFX v  er  itivissime/_Á er  ds:itiv ds:issim
SFX v  r  tivissime/_Á ir  ds:itiv ds:issim
SFX v  -  ivissime/_Á  t-  ds:iv ds:issim
SFX v  -  ivissime/_Á  [sx]- ds:iv ds:issim

SFX w Y 5
SFX w  r  tivitate/_S  ar  ds:ativ
SFX w  er  itivitate/_S er  ds:itiv
SFX w  r  tivitate/_S  ir  ds:itiv
SFX w  -  ivitate/_S  t-  ds:tiv
SFX w  -  ivitate/_S  [sx]- ds:siv
 
SFX x Y 19
SFX x  0  -un/_f   .
SFX x  0  -duo/_f   .
SFX x  0  -tres/_f  .
SFX x  0  -tre/_f   .
SFX x  0  -quatro/_f  .
SFX x  0  -cinque/_f  .
SFX x  0  -sex/_f   .
SFX x  0  -septe/_f  .
SFX x  0  -octo/_f  .
SFX x  0  -nove/_f  .
SFX x  0  -prime/_O  .
SFX x  0  -secunde/_O  .
SFX x  0  -tertie/_O  .
SFX x  0  -quarte/_O  .
SFX x  0  -quinte/_O  .
SFX x  0  -sexte/_O  .
SFX x  0  -septime/_O  .
SFX x  0  -octave/_O  .
SFX x  0  -none/_O  .

SFX y Y 19
SFX y  i  esimoprime/_O i
SFX y  i  esimosecunde/_O i
SFX y  i  esimotertie/_O i
SFX y  i  esimoquarte/_O i
SFX y  i  esimoquinte/_O i
SFX y  i  esimosexte/_O i
SFX y  i  esimoseptime/_O i
SFX y  i  esimoctave/_O i
SFX y  i  esimonone/_O i
SFX y  e  oprime/_O  cime
SFX y  e  osecunde/_O  cime
SFX y  e  otertie/_O  cime
SFX y  e  oquarte/_O  cime
SFX y  e  oquinte/_O  cime
SFX y  e  osexte/_O  cime
SFX y  e  oseptime/_O  cime
SFX y  e  octave/_O  cime
SFX y  e  onone/_O  cime
SFX y  e  odecime/_O  [^c]..e

PFX z Y 20
PFX z  0  un    .
PFX z  0  duo    .
PFX z  0  tre    [^coqtv]
PFX z  0  tres   [coqtv]
PFX z  0  quattor   .
PFX z  0  quattuor  .
PFX z  0  quin   de
PFX z  0  quinqua   du
PFX z  0  quinqua   [^d]
PFX z  0  se    [^qtvco]
PFX z  0  ses    [qtv] 
PFX z  0  sex    [co]
PFX z  0  septe   n
PFX z  0  septen   [^nov]
PFX z  0  septem   [ov]
PFX z  0  oct    o
PFX z  0  octo   [^o]
PFX z  0  nove   n
PFX z  0  noven   [^nov]
PFX z  0  novem   [ov]

ICONV 468 
ICONV à‧ a
ICONV á‧ a
ICONV â‧ a
ICONV ä‧ a
ICONV ã‧ a
ICONV ā‧ a
ICONV ă‧ a
ICONV ạ‧ a
ICONV ą‧ a
ICONV è‧ e
ICONV é‧ e
ICONV ê‧ e
ICONV ë‧ e
ICONV ē‧ e
ICONV ĕ‧ e
ICONV ẹ‧ e
ICONV ę‧ e
ICONV ì‧ i
ICONV í‧ i
ICONV î‧ i
ICONV ï‧ i
ICONV ĩ‧ i
ICONV ī‧ i
ICONV ĭ‧ i
ICONV į‧ i
ICONV ò‧ o
ICONV ó‧ o
ICONV ô‧ o
ICONV ö‧ o
ICONV õ‧ o
ICONV ō‧ o
ICONV ŏ‧ o
ICONV ọ‧ o
ICONV ǫ‧ o
ICONV ù‧ u
ICONV ú‧ u
ICONV û‧ u
ICONV ü‧ u
ICONV ũ‧ u
ICONV ū‧ u
ICONV ŭ‧ u
ICONV ụ‧ u
ICONV ų‧ u
ICONV ỳ‧ y
ICONV ý‧ y
ICONV ŷ‧ y
ICONV ÿ‧ y
ICONV ỹ‧ y
ICONV ȳ‧ y
ICONV ỵ‧ y
ICONV À‧ A
ICONV Á‧ A
ICONV Â‧ A
ICONV Ä‧ A
ICONV Ã‧ A
ICONV Ā‧ A
ICONV Ă‧ A
ICONV Ạ‧ A
ICONV Ą‧ A
ICONV È‧ E
ICONV É‧ E
ICONV Ê‧ E
ICONV Ë‧ E
ICONV Ē‧ E
ICONV Ĕ‧ E
ICONV Ẹ‧ E
ICONV Ę‧ E
ICONV Ì‧ I
ICONV Í‧ I
ICONV Î‧ I
ICONV Ï‧ I
ICONV Ĩ‧ I
ICONV Ī‧ I
ICONV Ĭ‧ I
ICONV Į‧ I
ICONV Ò‧ O
ICONV Ó‧ O
ICONV Ô‧ O
ICONV Ö‧ O
ICONV Õ‧ O
ICONV Ō‧ O
ICONV Ŏ‧ O
ICONV Ọ‧ O
ICONV Ǫ‧ O
ICONV Ù‧ U
ICONV Ú‧ U
ICONV Û‧ U
ICONV Ü‧ U
ICONV Ũ‧ U
ICONV Ū‧ U
ICONV Ŭ‧ U
ICONV Ụ‧ U
ICONV Ų‧ U
ICONV Ỳ‧ Y
ICONV Ý‧ Y
ICONV Ŷ‧ Y
ICONV Ÿ‧ Y
ICONV Ỹ‧ Y
ICONV Ȳ‧ Y
ICONV Ỵ‧ Y
ICONV ạ‧ a
ICONV ẹ‧ e
ICONV ị‧ i
ICONV ọ‧ o
ICONV ụ‧ u
ICONV ỵ‧ y
ICONV á‧ a
ICONV é‧ e
ICONV í‧ i
ICONV ó‧ o
ICONV ú‧ u
ICONV ý‧ y
ICONV à‧ a
ICONV è‧ e
ICONV ì‧ i
ICONV ò‧ o
ICONV ù‧ u
ICONV ỳ‧ y
ICONV â‧ a
ICONV ê‧ e
ICONV î‧ i
ICONV ô‧ o
ICONV û‧ u
ICONV ŷ‧ y
ICONV a̱‧ a
ICONV e̱‧ e
ICONV i̱‧ i
ICONV o̱‧ o
ICONV u̱‧ u
ICONV y̱‧ y
ICONV a̲‧ a
ICONV e̲‧ e
ICONV i̲‧ i
ICONV o̲‧ o
ICONV u̲‧ u
ICONV y̲‧ y
ICONV ā‧ a
ICONV ē‧ e
ICONV ī‧ i
ICONV ō‧ o
ICONV ū‧ u
ICONV ȳ‧ y
ICONV ă‧ a
ICONV ĕ‧ e
ICONV ĭ‧ i
ICONV ŏ‧ o
ICONV ŭ‧ u
ICONV y̆‧ y
ICONV a̯‧ a
ICONV e̯‧ e
ICONV i̯‧ i
ICONV o̯‧ o
ICONV u̯‧ u
ICONV y̯‧ y
ICONV ą‧ a
ICONV ę‧ e
ICONV į‧ i
ICONV ǫ‧ o
ICONV ų‧ u
ICONV y̨‧ y
ICONV ȧ‧ a
ICONV ė‧ e
ICONV i̇‧ i
ICONV ȯ‧ o
ICONV u̇‧ u
ICONV ẏ‧ y
ICONV ı‧ i
ICONV Ạ‧ A
ICONV Ẹ‧ E
ICONV Ị‧ I
ICONV Ọ‧ O
ICONV Ụ‧ U
ICONV Ỵ‧ Y
ICONV Á‧ A
ICONV É‧ E
ICONV Í‧ I
ICONV Ó‧ O
ICONV Ú‧ U
ICONV Ý‧ Y
ICONV À‧ A
ICONV È‧ E
ICONV Ì‧ I
ICONV Ò‧ O
ICONV Ù‧ U
ICONV Ỳ‧ Y
ICONV Â‧ A
ICONV Ê‧ E
ICONV Î‧ I
ICONV Ô‧ O
ICONV Û‧ U
ICONV Ŷ‧ Y
ICONV A̱‧ A
ICONV E̱‧ E
ICONV I̱‧ I
ICONV O̱‧ O
ICONV U̱‧ U
ICONV Y̱‧ Y
ICONV A̲‧ A
ICONV E̲‧ E
ICONV I̲‧ I
ICONV O̲‧ O
ICONV U̲‧ U
ICONV Y̲‧ Y
ICONV Ā‧ A
ICONV Ē‧ E
ICONV Ī‧ I
ICONV Ō‧ O
ICONV Ū‧ U
ICONV Ȳ‧ Y
ICONV Ă‧ A
ICONV Ĕ‧ E
ICONV Ĭ‧ I
ICONV Ŏ‧ O
ICONV Ŭ‧ U
ICONV Y̆‧ Y
ICONV A̯‧ A
ICONV E̯‧ E
ICONV I̯‧ I
ICONV O̯‧ O
ICONV U̯‧ U
ICONV Y̯‧ Y
ICONV Ą‧ A
ICONV Ę‧ E
ICONV Į‧ I
ICONV Ǫ‧ O
ICONV Ų‧ U
ICONV Y̨‧ Y
ICONV Ȧ‧ A
ICONV Ė‧ E
ICONV İ‧ I
ICONV Ȯ‧ O
ICONV U̇‧ U
ICONV Ẏ‧ Y
ICONV İ‧ I
ICONV a‧ a
ICONV b‧ b
ICONV c‧ c
ICONV ç‧ c
ICONV d‧ d
ICONV e‧ e
ICONV f‧ f
ICONV g‧ g
ICONV h‧ h
ICONV i‧ i
ICONV j‧ j
ICONV k‧ k
ICONV l‧ l
ICONV m‧ m
ICONV n‧ n
ICONV ñ‧ n
ICONV o‧ o
ICONV p‧ p
ICONV q‧ q
ICONV r‧ r
ICONV s‧ s
ICONV t‧ t
ICONV u‧ u
ICONV v‧ v
ICONV w‧ w
ICONV x‧ x
ICONV y‧ y
ICONV z‧ z
ICONV A‧ A
ICONV B‧ B
ICONV C‧ C
ICONV Ç‧ C
ICONV D‧ D
ICONV E‧ E
ICONV F‧ F
ICONV G‧ G
ICONV H‧ H
ICONV I‧ I
ICONV J‧ J
ICONV K‧ K
ICONV L‧ L
ICONV M‧ M
ICONV N‧ N
ICONV Ñ‧ N
ICONV O‧ O
ICONV P‧ P
ICONV Q‧ Q
ICONV R‧ R
ICONV S‧ S
ICONV T‧ T
ICONV U‧ U
ICONV V‧ V
ICONV W‧ W
ICONV X‧ X
ICONV Y‧ Y
ICONV Z‧ Z
ICONV ā a
ICONV ă a
ICONV ạ a
ICONV ą a
ICONV ē e
ICONV ĕ e
ICONV ẹ e
ICONV ę e
ICONV ī i
ICONV ĭ i
ICONV ị i
ICONV į i
ICONV ō o
ICONV ŏ o
ICONV ọ o
ICONV ǫ o
ICONV ū u
ICONV ŭ u
ICONV ụ u
ICONV ų u
ICONV ȳ y
ICONV ỵ y
ICONV Ā A
ICONV Ă A
ICONV Ạ A
ICONV Ą A
ICONV Ē E
ICONV Ĕ E
ICONV Ẹ E
ICONV Ę E
ICONV Ī I
ICONV Ĭ I
ICONV Ị I
ICONV Į I
ICONV Ō O
ICONV Ŏ O
ICONV Ọ O
ICONV Ǫ O
ICONV Ū U
ICONV Ŭ U
ICONV Ụ U
ICONV Ų U
ICONV Ȳ Y
ICONV Ỵ Y
ICONV ạ a
ICONV ẹ e
ICONV ị i
ICONV ọ o
ICONV ụ u
ICONV ỵ y
ICONV á a
ICONV é e
ICONV í i
ICONV ó o
ICONV ú u
ICONV ý y
ICONV à a
ICONV è e
ICONV ì i
ICONV ò o
ICONV ù u
ICONV ỳ y
ICONV â a
ICONV ê e
ICONV î i
ICONV ô o
ICONV û u
ICONV ŷ y
ICONV a̱ a
ICONV e̱ e
ICONV i̱ i
ICONV o̱ o
ICONV u̱ u
ICONV y̱ y
ICONV a̲ a
ICONV e̲ e
ICONV i̲ i
ICONV o̲ o
ICONV u̲ u
ICONV y̲ y
ICONV ā a
ICONV ē e
ICONV ī i
ICONV ō o
ICONV ū u
ICONV ȳ y
ICONV ă a
ICONV ĕ e
ICONV ĭ i
ICONV ŏ o
ICONV ŭ u
ICONV y̆ y
ICONV a̯ a
ICONV e̯ e
ICONV i̯ i
ICONV o̯ o
ICONV u̯ u
ICONV y̯ y
ICONV ą a
ICONV ę e
ICONV į i
ICONV ǫ o
ICONV ų u
ICONV y̨ y
ICONV ȧ a
ICONV ė e
ICONV i̇ i
ICONV ȯ o
ICONV u̇ u
ICONV ẏ y
ICONV ı i
ICONV Ạ A
ICONV Ẹ E
ICONV Ị I
ICONV Ọ O
ICONV Ụ U
ICONV Ỵ Y
ICONV Á A
ICONV É E
ICONV Í I
ICONV Ó O
ICONV Ú U
ICONV Ý Y
ICONV À A
ICONV È E
ICONV Ì I
ICONV Ò O
ICONV Ù U
ICONV Ỳ Y
ICONV Â A
ICONV Ê E
ICONV Î I
ICONV Ô O
ICONV Û U
ICONV Ŷ Y
ICONV A̱ A
ICONV E̱ E
ICONV I̱ I
ICONV O̱ O
ICONV U̱ U
ICONV Y̱ Y
ICONV A̲ A
ICONV E̲ E
ICONV I̲ I
ICONV O̲ O
ICONV U̲ U
ICONV Y̲ Y
ICONV Ā A
ICONV Ē E
ICONV Ī I
ICONV Ō O
ICONV Ū U
ICONV Ȳ Y
ICONV Ă A
ICONV Ĕ E
ICONV Ĭ I
ICONV Ŏ O
ICONV Ŭ U
ICONV Y̆ Y
ICONV A̯ A
ICONV E̯ E
ICONV I̯ I
ICONV O̯ O
ICONV U̯ U
ICONV Y̯ Y
ICONV Ą A
ICONV Ę E
ICONV Į I
ICONV Ǫ O
ICONV Ų U
ICONV Y̨ Y
ICONV Ȧ A
ICONV Ė E
ICONV İ I
ICONV Ȯ O
ICONV U̇ U
ICONV Ẏ Y
ICONV İ I
# ICONV à a
# ICONV á a
# ICONV â a
# ICONV ä a
# ICONV ã a
# ICONV è e
# ICONV é e
# ICONV ê e
# ICONV ë e
# ICONV ì i
# ICONV í i
# ICONV î i
# ICONV ï i
# ICONV ĩ i
# ICONV ò o
# ICONV ó o
# ICONV ô o
# ICONV ö o
# ICONV õ o
# ICONV ù u
# ICONV ú u
# ICONV û u
# ICONV ü u
# ICONV ũ u
# ICONV ỳ y
# ICONV ý y
# ICONV ŷ y
# ICONV ÿ y
# ICONV ỹ y
# ICONV À A
# ICONV Á A
# ICONV Â A
# ICONV Ä A
# ICONV Ã A
# ICONV È E
# ICONV É E
# ICONV Ê E
# ICONV Ë E
# ICONV Ì I
# ICONV Í I
# ICONV Î I
# ICONV Ï I
# ICONV Ĩ I
# ICONV Ò O
# ICONV Ó O
# ICONV Ô O
# ICONV Ö O
# ICONV Õ O
# ICONV Ù U
# ICONV Ú U
# ICONV Û U
# ICONV Ü U
# ICONV Ũ U
# ICONV Ỳ Y
# ICONV Ý Y
# ICONV Ŷ Y
# ICONV Ÿ Y
# ICONV Ỹ Y
